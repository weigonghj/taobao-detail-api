# 淘宝商品详情API接口

#### 介绍
通过该API可快速获商品的标题、价格、SKU、主图、评价等完整信息，便于一些电商平台、系统使用。



#### 软件架构
软件采用负载均衡的架构，提供RESTFUL API，支持JAVA、PHP、PYTHON等编程语言使用

#### 请求示例

```
url = url+"productId=" + productId;
         SyncHttpTask sync = OkHttps.sync(url);
        sync.addHeader("token","联系我拿token");//联系方式：qq:541645121 wechat:13318822059
        
        HttpResult httpResult = sync.post();
        String rs = httpResult.getBody().toString();
```


#### 使用说明

接口测试地址：URL: https://xxx/api/product/product

请求方式：GET

HEADER：token 

RESPONSE返回数据一览：

```
{
    "msg": "success",
    "code": 0,
    "data": {
        "seller": {
            "newItemCount": "328",
            "shopCard": "掌柜近期上新328件宝贝，速览",
            "shopName": "时然女装品牌店",
            "certIcon": "http://gw.alicdn.com/tfs/TB1zgzmlZLJ8KJjy0FnXXcFDpXa-171-148.png",
            "evaluates": [
                {
                    "score": "4.8 ",
                    "tmallLevelTextColor": "#FF0036",
                    "level": "1",
                    "levelText": "高",
                    "levelBackgroundColor": "#FFF1EB",
                    "levelTextColor": "#FF5000",
                    "title": "宝贝描述",
                    "type": "desc",
                    "tmallLevelBackgroundColor": "#FFF1EB"
                },
                {
                    "score": "4.9 ",
                    "tmallLevelTextColor": "#FF0036",
                    "level": "1",
                    "levelText": "高",
                    "levelBackgroundColor": "#FFF1EB",
                    "levelTextColor": "#FF5000",
                    "title": "卖家服务",
                    "type": "serv",
                    "tmallLevelBackgroundColor": "#FFF1EB"
                },
                {
                    "score": "4.9 ",
                    "tmallLevelTextColor": "#FF0036",
                    "level": "1",
                    "levelText": "高",
                    "levelBackgroundColor": "#FFF1EB",
                    "levelTextColor": "#FF5000",
                    "title": "物流服务",
                    "type": "post",
                    "tmallLevelBackgroundColor": "#FFF1EB"
                }
            ],
            "entranceList": [
                {
                    "borderColor": "#59ffffff",
                    "backgroundColor": "#59000000",
                    "action": [
                        {
                            "params": {
                                "url": "//shop.m.taobao.com/shop/shop_index.htm?user_id=1753165376&item_id=652647967499&currentClickTime=-1"
                            },
                            "key": "open_url"
                        },
                        {
                            "params": {
                                "trackParams": {
                                    "spm": "a.2141.7631564.shoppage"
                                },
                                "trackName": "Button-NewShopcard-ShopPage"
                            },
                            "key": "user_track"
                        }
                    ],
                    "text": "进店逛逛",
                    "textColor": "#ffffff"
                },
                {
                    "borderColor": "#59ffffff",
                    "backgroundColor": "#59000000",
                    "action": [
                        {
                            "params": {
                                "url": "//shop.m.taobao.com/shop/shop_index.htm?user_id=1753165376&item_id=652647967499&shop_navi=allitems"
                            },
                            "key": "open_url"
                        },
                        {
                            "params": {
                                "trackParams": {
                                    "spm": "a.2141.7631564.allitem"
                                },
                                "trackName": "Button-NewShopcard-AllItem"
                            },
                            "key": "user_track"
                        }
                    ],
                    "text": "全部宝贝",
                    "textColor": "#ffffff"
                }
            ],
            "certText": "金牌卖家",
            "showShopLinkIcon": false,
            "simpleShopDOStatus": "1",
            "fbt2User": "希诺衣语",
            "taoShopUrl": "//shop.m.taobao.com/shop/shop_index.htm?user_id=1753165376&item_id=652647967499",
            "creditLevel": "14",
            "evaluates2": [
                {
                    "score": "4.8 ",
                    "titleColor": "#ffffff",
                    "level": "1",
                    "scoreTextColor": "#ffffff",
                    "levelText": "高",
                    "levelTextColor": "#f0f0f0",
                    "title": "宝贝描述",
                    "type": "desc"
                },
                {
                    "score": "4.9 ",
                    "titleColor": "#ffffff",
                    "level": "1",
                    "scoreTextColor": "#ffffff",
                    "levelText": "高",
                    "levelTextColor": "#f0f0f0",
                    "title": "卖家服务",
                    "type": "serv"
                },
                {
                    "score": "4.9 ",
                    "titleColor": "#ffffff",
                    "level": "1",
                    "scoreTextColor": "#ffffff",
                    "levelText": "高",
                    "levelTextColor": "#f0f0f0",
                    "title": "物流服务",
                    "type": "post"
                }
            ],
            "allItemCount": "1270",
            "creditLevelIcon": "//gw.alicdn.com/tfs/TB1Ag2CixrI8KJjy0FpXXb5hVXa-132-24.png",
            "atmosphereColor": "#ffffff",
            "shopId": "105394943",
            "starts": "2013-07-17 13:42:34",
            "sellerNick": "希诺衣语",
            "goodRatePercentage": "99.25%",
            "shopIcon": "https://img.alicdn.com/imgextra/i2/6000000005268/O1CN01gA9J191omnAiAdb2i_!!6000000005268-0-shopmanager.jpg",
            "isShowCertIcon": true,
            "shopUrl": "tmall://page.tm/shop?item_id=652647967499&shopId=105394943",
            "userId": "1753165376",
            "shopVersion": "0",
            "fans": "12.1万",
            "atmophereMask": false,
            "atmosphereImg": "https://img.alicdn.com/imgextra/i2/1753165376/O1CN014VmgbO1paFskCLwla_!!1753165376.jpg",
            "shopType": "C",
            "sellerType": "C",
            "shopTextColor": "#ffffff"
        },
        "propsCut": "适用年龄 材质 尺码 颜色分类 货号 成分含量 上市年份季节 ",
        "item": {
            "images": [
                "//img.alicdn.com/imgextra/i1/1753165376/O1CN01R92LjO1paFxfshPPJ_!!0-item_pic.jpg",
                "//img.alicdn.com/imgextra/i3/1753165376/O1CN01uP66x61paFwjps5mU_!!1753165376.jpg",
                "//img.alicdn.com/imgextra/i3/1753165376/O1CN01aVmBuN1paFwighY0m_!!1753165376.jpg",
                "//img.alicdn.com/imgextra/i2/1753165376/O1CN01tGqQ4b1paFwigeGF1_!!1753165376.jpg",
                "//img.alicdn.com/imgextra/i1/1753165376/O1CN01Jqu0HF1paFx1rvwyw_!!1753165376.jpg"
            ],
            "tmallDescUrl": "//mdetail.tmall.com/templates/pages/desc?id=652647967499",
            "favcount": "4498",
            "countMultiple": [],
            "cartUrl": "https://h5.m.taobao.com/awp/base/cart.htm",
            "exParams": {},
            "title": "休闲套装女2021年秋季新款女装时尚复古洋气早秋显瘦运动两件套潮",
            "openDecoration": false,
            "rootCategoryId": "16",
            "commentCount": "770",
            "itemId": "652647967499",
            "skuText": "请选择颜色分类 尺码 ",
            "taobaoDescUrl": "//h5.m.taobao.com/app/detail/desc.html?_isH5Des=true#!id=652647967499&type=0&f=icoss724920844f4de55b80aa2fff&sellerType=C",
            "taobaoPcDescUrl": "//h5.m.taobao.com/app/detail/desc.html?_isH5Des=true#!id=652647967499&type=1&f=icoss265195114808a2e3872d2d26d&sellerType=C",
            "categoryId": "123216004"
        },
        "debug": {
            "app": "alidetail",
            "host": "detail033060156068.center.na620@33.60.156.68"
        },
        "resource": {
            "entrances": {
                "askAll": {
                    "icon": "https://img.alicdn.com/tps/TB1tVU6PpXXXXXFaXXXXXXXXXXX-102-60.png",
                    "link": "https://web.m.taobao.com/app/mtb/ask-everyone/list?pha=true&disableNav=YES&refId=652647967499",
                    "text": "\"请问，1.66米，136斤穿什么尺寸合适？\""
                }
            }
        },
        "vertical": {
            "askAll": {
                "showNum": "2",
                "askIcon": "https://img.alicdn.com/tps/TB1tVU6PpXXXXXFaXXXXXXXXXXX-102-60.png",
                "askText": "请问，1.66米，136斤穿什么尺寸合适？",
                "modelList": [
                    {
                        "askText": "请问，1.66米，136斤穿什么尺寸合适？",
                        "answerCountText": "2个回答",
                        "firstAnswer": "洗了要缩水，买XL或者XXL"
                    },
                    {
                        "askText": "买过的亲xxL能穿多少斤以内",
                        "answerCountText": "5个回答",
                        "firstAnswer": "xl的140斤能穿，买回太肥懒得退，送人了"
                    }
                ],
                "answerText": "洗了要缩水，买XL或者XXL",
                "questNum": "9",
                "linkUrl": "https://web.m.taobao.com/app/mtb/ask-everyone/list?pha=true&disableNav=YES&refId=652647967499",
                "model4XList": [
                    {
                        "askIcon": "//gw.alicdn.com/tfs/TB1lneilZLJ8KJjy0FnXXcFDpXa-36-36.png",
                        "askText": "请问，1.66米，136斤穿什么尺寸合适？",
                        "askTextColor": "#162B36",
                        "answerCountText": "2个回答"
                    },
                    {
                        "askIcon": "//gw.alicdn.com/tfs/TB1lneilZLJ8KJjy0FnXXcFDpXa-36-36.png",
                        "askText": "买过的亲xxL能穿多少斤以内",
                        "askTextColor": "#162B36",
                        "answerCountText": "5个回答"
                    }
                ],
                "title": "问大家(9)",
                "answerIcon": "https://img.alicdn.com/tps/TB1Z7c2LXXXXXXmaXXXXXXXXXXX-132-42.png"
            }
        },
        "params": {
            "trackParams": {
                "BC_type": "C",
                "categoryId": "123216004"
            }
        },
        "apiStack": [
            {
                "name": "esi",
                "value": "{\"delivery\":{\"from\":\"浙江杭州\",\"to\":\"江苏连云港\",\"completedTo\":\"连云港\",\"areaId\":\"320700\",\"postage\":\"快递: 快递包邮\",\"extras\":{},\"overseaContraBandFlag\":\"false\",\"addressWeexUrl\":\"https://market.m.taobao.com/apps/market/detailrax/address-picker.html?spm=a2116h.app.0.0.16d957e9nDYOzv&wh_weex=true\"},\"item\":{\"titleIcon\":\"\",\"showShopActivitySize\":\"2\",\"vagueSellCount\":\"200+\",\"skuText\":\"请选择 颜色分类 尺码 \"},\"priceSectionData\":{\"mainBelt\":{\"styleType\":\"0\",\"bizType\":\"0\",\"rightBelt\":{\"countdown\":\"0\",\"text\":\"\",\"textColor\":\"#FFFFFF\",\"extraTextColor\":\"#FFFFFF\"}},\"price\":{\"priceMoney\":\"18900\",\"priceText\":\"189\",\"priceTitle\":\"大减价\",\"newLine\":\"false\",\"priceType\":\"origin_price\"},\"priceType\":\"line_price\",\"extraPrice\":{\"priceMoney\":\"29900\",\"priceText\":\"299\",\"priceTitle\":\"价格\",\"priceColor\":\"#999999\",\"lineThrough\":\"true\",\"newLine\":\"false\",\"priceType\":\"line_price\"},\"promotion\":{\"entranceTip\":\"查看\",\"items\":[{\"textColor\":\"#FD5F20\",\"content\":\"淘金币可抵5.67元\",\"type\":\"default\",\"bgImage\":\"https://gw.alicdn.com/tfs/TB1.dqZSgHqK1RjSZJnXXbNLpXa-40-40.png\",\"sbgImage\":\"https://gw.alicdn.com/tfs/TB12R2Oerj1gK0jSZFuXXcrHpXa-302-80.png\",\"scontent\":\"淘金币可抵5.67元\"},{\"textColor\":\"#FD5F20\",\"content\":\"299元任选2件\",\"type\":\"default\",\"bgImage\":\"https://gw.alicdn.com/tfs/TB1.dqZSgHqK1RjSZJnXXbNLpXa-40-40.png\",\"sbgImage\":\"https://gw.alicdn.com/tfs/TB12R2Oerj1gK0jSZFuXXcrHpXa-302-80.png\",\"scontent\":\"299元任选2件\"}],\"entranceUrl\":\"https://market.m.taobao.com/app/detail-project/detail-pages/pages/quan?wh_weex=true\",\"promotionStyle\":\"false\"}},\"debug\":{\"host\":\"alidetail033050254199.center.na620@33.50.254.199\",\"app\":\"alidetail\"},\"traceDatas\":{\"dinamic+TB_detail_ask_all_two_questions\":{\"module\":\"tb_detail_ask_all_two_questions\"},\"dinamic+TB_detail_brand_info\":{\"module\":\"tb_detail_brand_info\"},\"dinamic+TB_detail_endorsement\":{\"module\":\"tb_detail_endorsement\"},\"dinamic+TB_detail_tip_presale\":{\"module\":\"tb_detail_tip_presale\"},\"dinamic+TB_detail_subInfo_superMarket\":{\"module\":\"tb_detail_subInfo_superMarket\"},\"bizTrackParams\":{\"aliBizCodeToken\":\"YWxpLmNoaW5hLnRhb2Jhbw==\",\"aliBizCode\":\"ali.china.taobao\"},\"dinamic+TB_detail_ask_all_no_question\":{\"module\":\"tb_detail_ask_all_no_question\"},\"dinamic+TB_detail_kernel_params\":{\"module\":\"tb_detail_kernel_params\"},\"dinamic_o2o+TB_detail_o2o\":{\"module\":\"TB_detail_o2o\"},\"dinamic+TB_detail_subInfo_jhs_normal\":{\"module\":\"tb_detail_subInfo_jhs_normal\"},\"dinamic+TB_detail_new_person_bag_banner\":{\"module\":\"tb_detail_new_person_bag_banner\"},\"dinamic+TB_detail_title_tmallMarket\":{\"module\":\"tb_detail_title_tmallMarket\"},\"dinamic+TB_detail_buyer_photo\":{\"module\":\"tb_detail_buyer_photo\"},\"dinamic+TB_detail_subInfo_preSellForTaobaoB\":{\"module\":\"tb_detail_subInfo_preSellForTaobaoB\"},\"dinamic+TB_detail_title_xinxuan\":{\"module\":\"tb_detail_title_xinxuan\"},\"dinamic+TB_detail_subInfo_preSellForTaobaoC\":{\"module\":\"tb_detail_subInfo_preSellForTaobaoC\"},\"dinamic+TB_detail_tmallfeature\":{\"module\":\"tb_detail_tmallfeature\"},\"dinamic+TB_detail_sub_logistics\":{\"module\":\"tb_detail_sub_logistics\"},\"dinamic+TB_detail_price_coupon\":{\"module\":\"tb_detail_price_coupon\"},\"dinamic+TB_detail_coupon\":{\"module\":\"tb_detail_coupon\"},\"dinamic+TB_detail_tip_presale2\":{\"module\":\"tb_detail_tip_presale2\"},\"dinamic+TB_detail_tip_price\":{\"module\":\"tb_detail_tip_price\"},\"dinamic+TB_detail_delivery\":{\"module\":\"tb_detail_delivery\"},\"dinamic+TB_detail_ship_time\":{\"module\":\"tb_detail_ship_time\"},\"dinamic+TB_detail_ask_all_aliMedical\":{\"module\":\"tb_detail_ask_all_aliMedical\"},\"dinamic+TB_detail_priced_coupon\":{\"module\":\"tb_detail_priced_coupon\"},\"dinamic+TB_detail_tax\":{\"module\":\"tb_detail_tax\"},\"dinamic+TB_detail_comment_empty\":{\"module\":\"tb_detail_comment_empty\"},\"dinamic+TB_detail_logistics\":{\"module\":\"tb_detail_logistics\"}},\"resource\":{\"newBigPromotion\":{},\"entrances\":{},\"coupon\":{\"h5LinkUrl\":\"//h5.m.taobao.com/jxt/coupon-tb.html\"},\"promsCalcInfo\":{\"hasCoupon\":\"false\",\"cheapestMoney\":\"0\"},\"floatView\":{\"list\":[]}},\"consumerProtection\":{\"items\":[{\"title\":\"7天无理由\",\"desc\":\"满足7天无理由退换货申请的前提下，包邮商品需要买家承担退货邮费，非包邮商品需要买家承担发货和退货邮费。\"},{\"title\":\"运费险\",\"desc\":\"卖家为您购买的商品投保退货运费险（保单生效以确认订单页展示的运费险为准）\"},{\"title\":\"蚂蚁花呗\"},{\"title\":\"信用卡支付\"},{\"title\":\"集分宝\"}],\"passValue\":\"all\"},\"skuCore\":{\"sku2info\":{\"0\":{\"price\":{\"priceMoney\":\"18900\",\"priceText\":\"189\",\"type\":\"1\"},\"quantity\":\"307\"},\"4700782768139\":{\"price\":{\"priceMoney\":\"18900\",\"priceText\":\"189\",\"type\":\"1\"},\"quantity\":\"4\"},\"4700782768142\":{\"price\":{\"priceMoney\":\"18900\",\"priceText\":\"189\",\"type\":\"1\"},\"quantity\":\"89\"},\"4700782768143\":{\"price\":{\"priceMoney\":\"18900\",\"priceText\":\"189\",\"type\":\"1\"},\"quantity\":\"90\"},\"4700782768140\":{\"price\":{\"priceMoney\":\"18900\",\"priceText\":\"189\",\"type\":\"1\"},\"quantity\":\"28\"},\"4700782768141\":{\"price\":{\"priceMoney\":\"18900\",\"priceText\":\"189\",\"type\":\"1\"},\"quantity\":\"96\"}},\"skuItem\":{\"location\":\"江苏连云港\"},\"abSwitch\":{},\"atmosphere\":{}},\"tradeConsumerProtection\":{\"tradeConsumerService\":{\"service\":{\"items\":[{\"icon\":\"//gw.alicdn.com/tfs/TB1O4sFQpXXXXb3apXXXXXXXXXX-200-200.png\",\"title\":\"7天无理由\",\"desc\":\"满足7天无理由退换货申请的前提下，包邮商品需要买家承担退货邮费，非包邮商品需要买家承担发货和退货邮费。\"},{\"icon\":\"//gw.alicdn.com/tfs/TB1O4sFQpXXXXb3apXXXXXXXXXX-200-200.png\",\"title\":\"运费险\",\"desc\":\"卖家为您购买的商品投保退货运费险（保单生效以确认订单页展示的运费险为准）\"}],\"icon\":\"\",\"title\":\"基础服务\"},\"nonService\":{\"items\":[{\"icon\":\"//gw.alicdn.com/tfs/TB1O4sFQpXXXXb3apXXXXXXXXXX-200-200.png\",\"title\":\"蚂蚁花呗\"},{\"icon\":\"//gw.alicdn.com/tfs/TB1O4sFQpXXXXb3apXXXXXXXXXX-200-200.png\",\"title\":\"信用卡支付\"},{\"icon\":\"//gw.alicdn.com/tfs/TB1O4sFQpXXXXb3apXXXXXXXXXX-200-200.png\",\"title\":\"集分宝\"}],\"title\":\"其他\"}},\"passValue\":\"all\",\"url\":\"https://h5.m.taobao.com/app/detailsubpage/consumer/index.js\",\"type\":\"0\"},\"vertical\":{\"groupChat\":{\"image\":\"//gw.alicdn.com/tfs/TB1Kg_MdoGF3KVjSZFmXXbqPXXa-164-52.png\",\"fansCount\":\"多\",\"title\":\"群主不定时发放专享福利\",\"url\":\"//h5.m.taobao.com/global/open_group_chat/index.html?from=default_detailcard&mixSellerId=XFZ-vmvuMk8SvCvGPmZIv0xLMmhIOH*zvGQWXHPhM0gT\",\"desc\":\"加入群\"},\"askAll\":{\"askText\":\"请问，1.66米，136斤穿什么尺寸合适？\",\"askIcon\":\"https://img.alicdn.com/tps/TB1tVU6PpXXXXXFaXXXXXXXXXXX-102-60.png\",\"answerText\":\"洗了要缩水，买XL或者XXL\",\"answerIcon\":\"https://img.alicdn.com/tps/TB1Z7c2LXXXXXXmaXXXXXXXXXXX-132-42.png\",\"linkUrl\":\"https://web.m.taobao.com/app/mtb/ask-everyone/list?pha=true&disableNav=YES&refId=652647967499\",\"title\":\"问大家(9)\",\"questNum\":\"9\",\"showNum\":\"2\",\"modelList\":[{\"askText\":\"请问，1.66米，136斤穿什么尺寸合适？\",\"answerCountText\":\"2个回答\",\"firstAnswer\":\"洗了要缩水，买XL或者XXL\"},{\"askText\":\"买过的亲xxL能穿多少斤以内\",\"answerCountText\":\"5个回答\",\"firstAnswer\":\"xl的140斤能穿，买回太肥懒得退，送人了\"}]}},\"params\":{\"trackParams\":{\"spm\":null,\"aliBizCodeToken\":\"YWxpLmNoaW5hLnRhb2Jhbw==\",\"detailabtestdetail\":\"158147_26416.5154_4504.167602_28767.112824_22301.161974_30993\",\"businessTracks\":\"%7B%22158147%22%3A%2226416%22%2C%22167602%22%3A%2228767%22%2C%22112824%22%3A%2222301%22%2C%22161974%22%3A%2230993%22%7D\",\"detailUniqueId\":\"0536a31298c503cfbf242e7a840f6113\",\"layoutId\":null,\"aliBizCode\":\"ali.china.taobao\"},\"aliAbTestTrackParams\":{\"recommend2018\":\"[{\\\"abtest\\\":\\\"5154_4504\\\",\\\"component\\\":\\\"recommendNewVersion\\\",\\\"releaseId\\\":5154,\\\"module\\\":\\\"2018\\\",\\\"cm\\\":\\\"recommendNewVersion_2018\\\",\\\"experimentId\\\":2021,\\\"bucketId\\\":4504,\\\"trackConfigs\\\":\\\"[]\\\"}]\"},\"umbParams\":{\"aliBizName\":\"ali.china.taobao\",\"aliBizCode\":\"ali.china.taobao\"}},\"promotionFloatingData\":{\"detailPromotionTimeDO\":{\"promotionType\":\"NLORMAL\"},\"showWarm\":\"true\",\"showNow\":\"true\"},\"layout\":{},\"weappData\":{},\"hybrid\":{\"shopRecommendItems\":{\"url\":\"https://market.m.taobao.com/apps/market/detailrax/recommend-shop-bigpage.html?spm=a2116h.app.0.0.16d957e9B7oLGw&wh_weex=true&sellerId=1753165376&itemId=652647967499&detail_v=3.1.8&selfRmdFlag=true\",\"height\":\"445\",\"spm\":\"\"}},\"trade\":{\"buyEnable\":\"true\",\"cartEnable\":\"true\",\"buyParam\":{},\"cartParam\":{},\"isBanSale4Oversea\":\"false\",\"buyUrl\":\"buildOrderVersion=3.0\",\"cartJumpUrl\":\"https://h5.m.taobao.com/awp/base/cart.htm\"},\"feature\":{\"dailyPrice\":\"true\",\"openAddOnTools\":\"false\",\"enableDpbModule\":\"false\",\"showGroupChat\":\"true\",\"promotion2019\":\"true\",\"promotion2018\":\"true\",\"hasSku\":\"true\",\"hasNewCombo\":\"false\",\"freshmanRedPacket\":\"true\",\"noShareGroup\":\"true\",\"newPrice\":\"true\",\"superActTime\":\"false\",\"cainiaoNoramal\":\"true\",\"showSku\":\"true\",\"newAddress\":\"true\"},\"price\":{\"price\":{\"priceMoney\":\"18900\",\"priceText\":\"189\",\"type\":\"1\"},\"extraPrices\":[{\"priceMoney\":\"29900\",\"priceText\":\"299\",\"priceTitle\":\"价格\",\"type\":\"2\",\"lineThrough\":\"true\"}],\"priceTag\":[{\"text\":\"大减价\"},{\"text\":\"淘金币可抵5.67元\",\"bgColor\":\"#ff9204\"}],\"transmitPrice\":{\"priceText\":\"189\"}},\"diversion\":{\"productRecommend\":{\"request\":{\"api\":\"mtop.relationrecommend.WirelessRecommend.recommend\",\"params\":{\"params\":\"{\\\"itemid\\\":652647967499,\\\"spm\\\":\\\"0.0.0.0.hUro0P\\\",\\\"sellerid\\\":1753165376,\\\"appId\\\":\\\"10777\\\"}\",\"from\":\"dinamicX\",\"appId\":\"10777\"},\"version\":\"2.0\"},\"template\":{\"android\":\"https://ossgw.alicdn.com/rapid-oss-bucket/template_online/tb_shop_recommend/64005743/tb_shop_recommend_android.xml\",\"name\":\"tb_shop_recommend\",\"ios\":\"https://ossgw.alicdn.com/rapid-oss-bucket/template_online/tb_shop_recommend/64005743/tb_shop_recommend_ios.plist\",\"version\":\"37\"},\"preloadUrl\":\"https://gw.alicdn.com/tfs/TB1PS8nBAPoK1RjSZKbXXX1IXXa-1125-1335.png?getAvatar=avatar\"}},\"gallery\":{},\"extendedData\":{},\"skuVertical\":{}}"
            }
        ],
        "props": {
            "groupProps": [
                {
                    "基本信息": [
                        {
                            "适用年龄": "25-35周岁"
                        },
                        {
                            "材质": "棉"
                        },
                        {
                            "尺码": "S,M,L,XL,XXL"
                        },
                        {
                            "颜色分类": "浅灰色"
                        },
                        {
                            "货号": "G1QG8A07580"
                        },
                        {
                            "成分含量": "51%(含)-70%(含)"
                        },
                        {
                            "上市年份季节": "2021年秋季"
                        }
                    ]
                }
            ]
        },
        "mockData": "{\"delivery\":{},\"trade\":{\"buyEnable\":true,\"cartEnable\":true},\"feature\":{\"hasSku\":true,\"showSku\":true},\"price\":{\"price\":{\"priceText\":\"299.00\"}},\"skuCore\":{\"sku2info\":{\"0\":{\"price\":{\"priceMoney\":29900,\"priceText\":\"299.00\",\"priceTitle\":\"价格\"},\"quantity\":500},\"4700782768139\":{\"price\":{\"priceMoney\":29900,\"priceText\":\"299.00\",\"priceTitle\":\"价格\"},\"quantity\":100},\"4700782768142\":{\"price\":{\"priceMoney\":29900,\"priceText\":\"299.00\",\"priceTitle\":\"价格\"},\"quantity\":100},\"4700782768143\":{\"price\":{\"priceMoney\":29900,\"priceText\":\"299.00\",\"priceTitle\":\"价格\"},\"quantity\":100},\"4700782768140\":{\"price\":{\"priceMoney\":29900,\"priceText\":\"299.00\",\"priceTitle\":\"价格\"},\"quantity\":100},\"4700782768141\":{\"price\":{\"priceMoney\":29900,\"priceText\":\"299.00\",\"priceTitle\":\"价格\"},\"quantity\":100}},\"skuItem\":{\"hideQuantity\":true}}}",
        "feature": {
            "showSkuProRate": true
        },
        "rate": {
            "keywords": [
                {
                    "count": "3",
                    "attribute": "40001271-11",
                    "type": "1",
                    "word": "厚薄度合适"
                },
                {
                    "count": "2",
                    "attribute": "40161017-11",
                    "type": "1",
                    "word": "不会褪色"
                },
                {
                    "count": "2",
                    "attribute": "40011275-11",
                    "type": "1",
                    "word": "没有色差"
                },
                {
                    "count": "35",
                    "attribute": "40031066-11",
                    "type": "1",
                    "word": "款式很时尚"
                },
                {
                    "count": "14",
                    "attribute": "40111016-11",
                    "type": "1",
                    "word": "适合休闲风"
                },
                {
                    "count": "3",
                    "attribute": "40101000-13",
                    "type": "-1",
                    "word": "尺码不合适"
                }
            ],
            "rateList": [
                {
                    "dateTime": "2021-10-11",
                    "feedId": "1151852395724",
                    "memberLevel": "7",
                    "createTimeInterval": "12天前",
                    "blackCardUserUrl": "//img.alicdn.com/tfs/TB1wrG1elv0gK0jSZKbXXbK2FXa-225-96.png",
                    "userName": "王**0",
                    "headPic": "//wwc.alicdn.com/avatar/getAvatar.do?userIdStr=vFkSPFHuXH*evF8LPmHSXmIuvmgWvG7IPFvGvC*IvHRHMFHLPkcSvmQYv0Q4vHcu&width=40&height=40&type=sns",
                    "skuInfo": "颜色分类:浅灰色[部分码10.2号发货];尺码:L",
                    "content": "套装款式简单大方，没有色差，很喜欢这种高级浅灰色，165/60穿这个尺码宽松，穿着很休闲减龄。 ",
                    "isVip": "true",
                    "tmallMemberLevel": "0"
                },
                {
                    "dateTime": "2021-10-22",
                    "feedId": "1152765358434",
                    "memberLevel": "5",
                    "createTimeInterval": "1天前",
                    "userName": "最**0",
                    "headPic": "//wwc.alicdn.com/avatar/getAvatar.do?userIdStr=vFkSPFHuXH*evF8LPmHSXmhzPFvWMGQLOmRzXmPHP0kWPkgbPkPzPmcbPFlHv8gb&width=40&height=40&type=sns",
                    "skuInfo": "颜色分类:浅灰色[部分码10.2号发货];尺码:L",
                    "content": "衣服质量很好，尺码标准穿着很合适",
                    "isVip": "false",
                    "tmallMemberLevel": "0"
                }
            ],
            "utFeedId": "1151852395724_1152765358434",
            "propRate": [],
            "invite": {
                "showInvite": "false",
                "inviteText": ""
            },
            "totalCount": "770"
        },
        "props2": {},
        "skuBase": {
            "skus": [
                {
                    "propPath": "20509:28314;1627207:28332",
                    "skuId": "4700782768139"
                },
                {
                    "propPath": "20509:28315;1627207:28332",
                    "skuId": "4700782768140"
                },
                {
                    "propPath": "20509:28316;1627207:28332",
                    "skuId": "4700782768141"
                },
                {
                    "propPath": "20509:28317;1627207:28332",
                    "skuId": "4700782768142"
                },
                {
                    "propPath": "20509:28318;1627207:28332",
                    "skuId": "4700782768143"
                }
            ],
            "props": [
                {
                    "values": [
                        {
                            "vid": "28314",
                            "name": "S"
                        },
                        {
                            "vid": "28315",
                            "name": "M"
                        },
                        {
                            "vid": "28316",
                            "name": "L"
                        },
                        {
                            "vid": "28317",
                            "name": "XL"
                        },
                        {
                            "vid": "28318",
                            "name": "XXL"
                        }
                    ],
                    "name": "尺码",
                    "pid": "20509"
                },
                {
                    "values": [
                        {
                            "vid": "28332",
                            "image": "//img.alicdn.com/imgextra/i4/1753165376/O1CN01HL3rL41paFwyVhLug_!!1753165376.jpg",
                            "name": "浅灰色 部分码10.2号发货"
                        }
                    ],
                    "name": "颜色分类",
                    "pid": "1627207"
                }
            ]
        }
    },
    "state": true
}
```

#### 参与贡献

